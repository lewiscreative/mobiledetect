<?php

class Lewis_Mobiledetect_Model_Observer {
	const LAYOUT_HANDLE_MOBILE = 'mobile';
	const LAYOUT_HANDLE_TABLET = 'tablet';

	public function addLayoutHandle(Varien_Event_Observer $observer) {
		$update = $observer->getEvent()->getLayout()->getUpdate();

		$helper = Mage::helper('mobiledetect');
		if ($helper->isMobile()) {
			$update->addHandle(self::LAYOUT_HANDLE_MOBILE);
		}
		else if ($helper->isTablet()) {
			$update->addHandle(self::LAYOUT_HANDLE_TABLET);
		}
	}
}
