<?php

# load Mobile_Detect class
require_once Mage::getModuleDir('', 'Lewis_Mobiledetect').DS.'Mobile_Detect.php';

class Lewis_Mobiledetect_Helper_Data extends Mage_Core_Helper_Abstract {
	const XML_PATH_RESPONSIVE_MODE = 'design/mobiledetect/responsive_mode';
	const XML_PATH_TABLET_AS_MOBILE = 'design/mobiledetect/treat_tablet_as_mobile';
	const XML_PATH_OVERRIDE_ALLOW = 'design/mobiledetect/override_allow';

	protected $m_d;
	protected $cfg_rm;
	protected $cfg_ttam;
	protected $cfg_oa;

	public function getDetector() {
		if (! $this->m_d) {
			$this->m_d = new Mobile_Detect;
		}
		return $this->m_d;
	}

	public function hasOverride() {
		if ($this->cfg_oa === null) {
			$this->cfg_oa = (bool)Mage::getStoreConfig(self::XML_PATH_OVERRIDE_ALLOW);
		}
		return $this->cfg_oa && Mage::helper('mobiledetect/cookie')->get();
	}

	public function inResponsiveMode() {
		if ($this->cfg_rm === null) {
			$this->cfg_rm = (bool)Mage::getStoreConfig(self::XML_PATH_RESPONSIVE_MODE);
		}
		return $this->cfg_rm;
	}

	public function shouldTreatTabletAsMobile() {
		if ($this->cfg_ttam === null) {
			$this->cfg_ttam = (bool)Mage::getStoreConfig(self::XML_PATH_TABLET_AS_MOBILE);
		}
		return $this->cfg_ttam;
	}

	public function isMobile() {
		if ($this->hasOverride()) {
			return false;
		}
		else if ($this->inResponsiveMode()) {
			return true;
		}
		return ($this->getDetector()->isMobile() && ! $this->getDetector()->isTablet()) || ($this->shouldTreatTabletAsMobile() && $this->getDetector()->isTablet());
	}

	public function isTablet() {
		if ($this->hasOverride()) {
			return false;
		}
		else if ($this->inResponsiveMode()) {
			return true;
		}
		return (! $this->shouldTreatTabletAsMobile()) && $this->getDetector()->isTablet();
	}
}
