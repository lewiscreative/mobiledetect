<?php

class Lewis_Mobiledetect_Helper_Cookie extends Mage_Core_Helper_Abstract {
	const XML_PATH_OVERRIDE_COOKIE = 'design/mobiledetect/override_cookie';
	const XML_PATH_OVERRIDE_LENGTH = 'design/mobiledetect/override_length';

	protected $ck_name;
	protected $ck_len;
	protected $ck_value;

	protected function getCookieName() {
		if (! $this->ck_name) {
			$this->ck_name = Mage::getStoreConfig(self::XML_PATH_OVERRIDE_COOKIE);
		}
		return $this->ck_name;
	}

	protected function getCookieLength() {
		if (! $this->ck_len) {
			$days = Mage::getStoreConfig(self::XML_PATH_OVERRIDE_LENGTH);
			$this->ck_len = 60*60*24*$days;
		}
		return $this->ck_len;
	}

	public function set($bool) {
		$name = $this->getCookieName();
		if ($bool) {
			$length = $this->getCookieLength();
			Mage::getSingleton('core/cookie')->set($name, true, $length);
			$this->ck_value = true;
		}
		else {
			Mage::getSingleton('core/cookie')->delete($name);
			$this->ck_value = false;
		}
	}

	public function get() {
		if ($this->ck_value === null) {
			$name = $this->getCookieName();
			$cookie = Mage::getSingleton('core/cookie')->get($name);
			$this->ck_value = (bool)$cookie;
		}
		return $this->ck_value;
	}

	public function getSetUrl() {
		return Mage::getUrl('mobiledetect/override/set');
	}

	public function getUnsetUrl() {
		return Mage::getUrl('mobiledetect/override/unset');
	}
}
