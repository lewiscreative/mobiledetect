<?php

class Lewis_Mobiledetect_Block_Html extends Mage_Page_Block_Html {
	const BODY_CLASS_MOBILE = 'mobile';
	const BODY_CLASS_TABLET = 'tablet';
	const BODY_CLASS_DESKTOP = 'desktop';

	public function __construct() {
		parent::__construct();
		$helper = Mage::helper('mobiledetect');
		if ($helper->isMobile()) {
			$this->addBodyClass(self::BODY_CLASS_MOBILE);
		}
		else if ($helper->isTablet()) {
			$this->addBodyClass(self::BODY_CLASS_TABLET);
		}
		else {
			$this->addBodyClass(self::BODY_CLASS_DESKTOP);
		}
	}
}
