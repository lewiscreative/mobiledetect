<?php

class Lewis_Mobiledetect_OverrideController extends Mage_Core_Controller_Front_Action {
	public function setAction() {
		Mage::helper('mobiledetect/cookie')->set(true);
		$this->_redirectReferer();
	}

	public function unsetAction() {
		Mage::helper('mobiledetect/cookie')->set(false);
		$this->_redirectReferer();
	}
}
