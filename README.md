# Lewis\_Mobiledetect

Magento module adding mobile and tablet detection based on Mobile\_Detect class by @serbanghita

https://github.com/serbanghita/Mobile-Detect

Original version of module by @sammahoney - updated by @annyfm

## Usage

In PHP:

	<?php
    $helper = Mage::helper('mobiledetect');
	if ($helper->isMobile()) {
		// Mobile only
	}
	else if ($helper->isTablet()) {
		// Tablet only
	}
	else {
		// Desktop only
	}

In layout XML:

	<?xml version="1.0"?>
	<layout>
		<mobile>
			<!-- Mobile layout updates -->
		</mobile>
		<tablet>
			<!-- Tablet layout updates -->
		</tablet>
	</layout>

If store configuration at `design/mobiledetect/treat_tablet_as_mobile` == Yes, tablet devices are treated as mobile devices i.e. `tablet` layout handle is not used and `Mage::helper('mobiledetect')->isTablet()` will return false.
